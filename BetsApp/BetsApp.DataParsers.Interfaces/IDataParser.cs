﻿namespace BetsApp.DataParsers.Interfaces
{
    public interface IDataParser
    {
        object ParseData(string data);
    }
}
