﻿using BetsApp.DTOs;

namespace BetsApp.PL
{
    public interface IClientNotifier
    {
        void Notifie(SportsRootDTO data);
    }
}
