﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace BetsApp.PL
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);

            var controllerFactory = new BetsAppControllerFactory();

            ControllerBuilder.Current.SetControllerFactory(
                controllerFactory);

            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
    }
}
