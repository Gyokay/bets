﻿$(function () {
    var update = $.connection.updateHub;

    update.client.updateSports = function (sports) {
       
    };

    update.client.updateEvents = function (ev) {
        //ev.forEach(function (e) {
        //    var eventElement = $(".event[value=" + e.EventId + "]");

        //    if (eventElement.length > 0) {
        //        eventElement[0].childNodes[0].nodeValue = e.Name;
        //        eventElement.css("color", "red");
        //    } else if ($("#sport[value=" + e.SportId + "]")) {
        //        var newEvent = $("<li></li>")
        //            .addClass("event")
        //            .val(e.EventId)
        //            .text(e.Name)
        //            .css("color", "green");

        //        $("#sport[value=" + e.SportId + "] ul.event").append(newEvent[0]);
        //    }
        //});
    };

    update.client.updateMatches = function (matches) {
        //matches.forEach(function (m) {
        //    var matchElement = $(".match[value=" + m.MatchId + "]");
        //    var newMatchText = m.Name + " - " + m.StartDate

        //    if (matchElement.length > 0) {
        //        matchElement[0].childNodes[0].nodeValue = newMatchText;
        //        matchElement.css("color", "red");
        //    } else if ($(".event[value=" + m.EventId + "]")) {
        //        var newMatch = $("<li></li>")
        //            .addClass("match")
        //            .val(m.MatchId)
        //            .text(newMatchText)
        //            .css("color", "green");

        //        $(".event[value=" + m.EventId + "] ul.match").append(newMatch[0]);
        //    }
        //});
    };

    update.client.updateBets = function (bets) {
        //bets.forEach(function (b) {
        //    var betElement = $(".bet[value=" + b.BetId + "]");

        //    if (betElement.length > 0) {
        //        betElement[0].childNodes[0].nodeValue = b.Name;
        //        betElement.css("color", "red");
        //    } else if ($(".match[value=" + b.MatchId + "]")) {
        //        var newBet = $("<li></li>")
        //            .addClass("bet")
        //            .val(b.BetId)
        //            .text(b.Name)
        //            .css("color", "green");

        //        $(".match[value=" + b.MatchId + "] ul.bet").append(newBet[0]);
        //    }
        //});
    };

    update.client.updateOdds = function (odds) {
        odds.forEach(function (o) {
            var specialBetValue = o.SpecialBetValue || "";
            var newText = o.Name + " / " + o.Value + " / " + specialBetValue;

            var oddElement = $(".odd[value=" + o.OddId + "]");

            if (oddElement.length > 0) {
                oddElement
                    .text(newText)
                    .css("color", "red");
            } else if ($(".bet[value=" + o.BetId + "]").length > 0) {
                var newOdd = $("<li></li>")
                    .addClass("odd")
                    .val(o.OddId)
                    .text(newText)
                    .css("color", "green");

                $(".bet[value=" + o.BetId + "] ul.odd").append(newOdd[0]);
            }
        });
    };

    $.connection.hub.start().done(function () {
    });
});
