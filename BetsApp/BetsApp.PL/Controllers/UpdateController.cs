﻿using BetsApp.BLL.Interfaces;
using BetsApp.DTOs;
using System.Collections.Generic;
using System.Web.Http;

namespace BetsApp.PL.Controllers
{
    [Authorize]
    public class UpdateController : ApiController
    {
        private readonly ISportService sportService;
        private readonly IClientNotifier clientNotifier;

        public UpdateController(
            ISportService sportService,
            IClientNotifier clientNotifier
            )
        {
            this.sportService = sportService;
            this.clientNotifier = clientNotifier;
        }

        // POST api/<controller>
        [HttpPost]
        [Authorize(Users = "admin@mail.com")]
        public void Post([FromBody]UpdatedItemIdsDTO updatedItemIds)
        {
            var sportsFromDB = new List<SportDTO>();

            foreach (var sportId in updatedItemIds.SportIds)
            {
                sportsFromDB.Add(sportService.GetSportById(sportId));
            }

            var eventsFromDB = new List<EventDTO>();

            foreach (var eventId in updatedItemIds.EventIds)
            {
                eventsFromDB.Add(sportService.GetEventById(eventId));
            }

            var matchesFromDB = new List<MatchDTO>();

            foreach (var matchId in updatedItemIds.MatchIds)
            {
                matchesFromDB.Add(sportService.GetMatchById(matchId));
            }

            var betsFromDb = new List<BetDTO>();

            foreach (var betId in updatedItemIds.BetIds)
            {
                betsFromDb.Add(sportService.GetBetById(betId));
            }

            var oddsFromDB = new List<OddDTO>();

            foreach (var oddId in updatedItemIds.OddIds)
            {
                oddsFromDB.Add(sportService.GetOddById(oddId));
            }


            var rootDTO = new SportsRootDTO
            {
                Sports = sportsFromDB,
                Events = eventsFromDB,
                Matches = matchesFromDB,
                Bets = betsFromDb,
                Odds = oddsFromDB
            };

            this.clientNotifier.Notifie(rootDTO);
        }
    }
}