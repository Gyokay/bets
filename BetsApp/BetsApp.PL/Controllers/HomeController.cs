﻿using BetsApp.BLL.Interfaces;
using BetsApp.DTOs;
using BetsApp.PL.Models;
using System.Linq;
using System.Web.Mvc;

namespace BetsApp.PL.Controllers
{
    public class HomeController : Controller
    {
        private ISportService sportService;

        public HomeController(ISportService sportService)
        {
            this.sportService = sportService;
        }

        public ActionResult Index()
        {
            var sport = Request.QueryString["sport"];

            var rootDTO = sportService.GetSports();

            var rootModel = MapDTOtoModels(rootDTO);
            rootModel.SelectedSport = sport;

            return View(rootModel);
        }

        public ActionResult ESports()
        {
            return View();
        }

        private SportsRootViewModel MapDTOtoModels(SportsRootDTO rootDTO)
        {
            var sportModels = (from s in rootDTO.Sports
                               select new SportViewModel
                               {
                                   SportId = s.SportId,
                                   Name = s.Name
                               }).ToList();

            var eventModels = (from e in rootDTO.Events
                               select new EventViewModel
                               {
                                   EventId = e.EventId,
                                   SportId = e.SportId,
                                   Name = e.Name
                               }).ToList();

            var matchModels = (from m in rootDTO.Matches
                               select new MatchViewModel
                               {
                                   MatchId = m.MatchId,
                                   EventId = m.EventId,
                                   Name = m.Name,
                                   StartDate = m.StartDate
                               }).ToList();

            var betModel = (from b in rootDTO.Bets
                            select new BetViewModel
                            {
                                BetId = b.BetId,
                                MatchId = b.MatchId,
                                Name = b.Name,
                                IsLive = b.IsLive
                            }).ToList();

            var oddModel = (from o in rootDTO.Odds
                            select new OddViewModel
                            {
                                OddId = o.OddId,
                                BetId = o.BetId,
                                Name = o.Name,
                                Value = o.Value,
                                SpecialBetValue = o.SpecialBetValue
                            }).ToList();

            var rootModel = new SportsRootViewModel
            {
                Sports = sportModels,
                Events = eventModels,
                Matches = matchModels,
                Bets = betModel,
                Odds = oddModel
            };

            return rootModel;
        }
    }
}
