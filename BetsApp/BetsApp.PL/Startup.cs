﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(BetsApp.PL.Startup))]

namespace BetsApp.PL
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            app.MapSignalR();
        }
    }
}
