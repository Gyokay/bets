﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Microsoft.Practices.Unity;
using BetsApp.BLL.Interfaces;
using BetsApp.BLL;
using BetsApp.DAL.Interfaces;
using BetsApp.DAL.Repositories;
using Microsoft.AspNet.SignalR;

namespace BetsApp.PL
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            var container = new UnityContainer();
            container.RegisterType<ISportRepository, SportRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IEventRepository, EventRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IMatchRepository, MatchRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IBetRepository, BetRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IOddRepository, OddRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ISportService, SportService>(new HierarchicalLifetimeManager());
            container.RegisterType<IClientNotifier, SignalRClientNotifier>(new HierarchicalLifetimeManager());
            config.DependencyResolver = new UnityResolver(container);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
