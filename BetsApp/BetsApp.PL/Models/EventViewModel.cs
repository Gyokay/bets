﻿namespace BetsApp.PL.Models
{
    public class EventViewModel
    {
        public string EventId { get; set; }

        public string Name { get; set; }

        public string SportId { get; set; }
    }
}