﻿using System.Collections.Generic;

namespace BetsApp.PL.Models
{
    public class SportsRootViewModel
    {
        public string SelectedSport { get; set; }

        public IList<SportViewModel> Sports { get; set; }

        public IList<EventViewModel> Events { get; set; }

        public IList<MatchViewModel> Matches { get; set; }

        public IList<BetViewModel> Bets { get; set; }

        public IList<OddViewModel> Odds { get; set; }
    }
}