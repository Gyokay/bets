﻿using System;

namespace BetsApp.PL.Models
{
    public class MatchViewModel
    {
        public string MatchId { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public string EventId { get; set; }
    }
}