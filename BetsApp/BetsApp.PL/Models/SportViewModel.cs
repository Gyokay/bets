﻿namespace BetsApp.PL.Models
{
    public class SportViewModel
    {
        public string SportId { get; set; }

        public string Name { get; set; }
    }
}