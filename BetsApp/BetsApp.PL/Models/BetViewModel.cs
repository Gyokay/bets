﻿namespace BetsApp.PL.Models
{
    public class BetViewModel
    {
        public string BetId { get; set; }

        public string Name { get; set; }

        public bool IsLive { get; set; }

        public string MatchId { get; set; }
    }
}