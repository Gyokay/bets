﻿using BetsApp.BLL;
using BetsApp.DAL;
using BetsApp.DAL.Repositories;
using BetsApp.PL.Controllers;
using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace BetsApp.PL
{
    public class BetsAppControllerFactory : DefaultControllerFactory
    {
        protected override IController GetControllerInstance(
            RequestContext requestContext, Type controllerType)
        {
            var db = new BetsContext();

            var sportRepo = new SportRepository(db);
            var eventRepo = new EventRepository(db);
            var matchRepo = new MatchRepository(db);
            var betRepo = new BetRepository(db);
            var oddRepo = new OddRepository(db);

            var sportService = new SportService(
                sportRepo,
                eventRepo,
                matchRepo,
                betRepo,
                oddRepo
                );

            if (controllerType == typeof(HomeController))
            {
                return new HomeController(sportService);
            }

            return base.GetControllerInstance(
            requestContext, controllerType);
        }
    }
}