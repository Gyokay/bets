﻿using BetsApp.DTOs; 
using Microsoft.AspNet.SignalR;
using System.Collections.Generic;

namespace BetsApp.PL
{
    public class SignalRClientNotifier : IClientNotifier
    {
        private IHubContext context;

        public SignalRClientNotifier()
        {
            this.context = GlobalHost.ConnectionManager.GetHubContext<UpdateHub>();
        }

        public void Notifie(SportsRootDTO data)
        {
            if (data.Sports.Count > 0)
            {
                SendSportsToClient(data.Sports);
            }

            if (data.Events.Count > 0)
            {
                SendEventsToClient(data.Events);
            }

            if (data.Matches.Count > 0)
            {
                SendMatchesToClient(data.Matches);
            }

            if (data.Bets.Count > 0)
            {
                SendBetsToClient(data.Bets);
            }

            if (data.Odds.Count > 0)
            {
                SendOddsToClient(data.Odds);
            }
        }

        private void SendSportsToClient(IList<SportDTO> data)
        {
            context.Clients.All.updateSports(data);
        }

        private void SendEventsToClient(IList<EventDTO> data)
        {
            context.Clients.All.updateEvents(data);
        }

        private void SendMatchesToClient(IList<MatchDTO> data)
        {
            context.Clients.All.updateMatches(data);
        }

        private void SendBetsToClient(IList<BetDTO> data)
        {
            context.Clients.All.updateBets(data);
        }

        private void SendOddsToClient(IList<OddDTO> data)
        {
            context.Clients.All.updateOdds(data);
        }
    }
}