﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetsApp.DAL.Models
{
    public class Match
    {
        public Match()
        {
            this.Bets = new HashSet<Bet>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string MatchId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime StartDate { get; set; }

        public string MatchType { get; set; }

        public virtual ICollection<Bet> Bets { get; set; }

        public string EventId { get; set; }
        public virtual Event Event { get; set; }
    }
}
