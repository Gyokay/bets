﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetsApp.DAL.Models
{
    public class Sport
    {
        public Sport()
        {
            this.Events = new HashSet<Event>();
            //this.Matches = new HashSet<Match>();
            //this.Bets = new HashSet<Bet>();
            //this.Odds = new HashSet<Odd>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string SportId { get; set; }

        [Required]
        public string Name { get; set; }

        public virtual ICollection<Event> Events { get; set; }

        //public virtual ICollection<Match> Matches { get; set; }

        //public virtual ICollection<Bet> Bets { get; set; }

        //public virtual ICollection<Odd> Odds { get; set; }
    }
}
