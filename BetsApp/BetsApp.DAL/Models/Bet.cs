﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetsApp.DAL.Models
{
    public class Bet
    {
        public Bet()
        {
            this.Odds = new HashSet<Odd>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string BetId { get; set; }

        [Required]
        public string Name { get; set; }

        public bool IsLive { get; set; }

        public virtual ICollection<Odd> Odds { get; set; }

        public string MatchId { get; set; }
        public virtual Match Match {get; set;}
    }
}
