﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BetsApp.DAL.Models
{
    public class Event
    {
        public Event()
        {
            this.Matches = new HashSet<Match>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string EventId { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string CategoryId { get; set; }

        public bool IsLive { get; set; }

        public virtual ICollection<Match> Matches { get; set; }

        public string SportId { get; set; }
        public virtual Sport Sport { get; set; }
    }
}
