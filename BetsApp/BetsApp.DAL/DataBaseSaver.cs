﻿using BetsApp.DAL.Interfaces;

namespace BetsApp.DAL
{
    public class DatabaseSaver : IDatabaseSaver
    {
        private BetsContext db;

        public DatabaseSaver(BetsContext db)
        {
            this.db = db;
        }

        internal void Save()
        {
            if (++ModifiedElementsTracker.ModifiedElementsCount ==
                ModifiedElementsTracker.MaxElementCount)
            {
                ModifiedElementsTracker.ModifiedElementsCount = 1;
                db.SaveChanges();
                System.Console.Write('.');
            }
        }

        public void SaveImmediately()
        {
            ModifiedElementsTracker.ModifiedElementsCount = 1;
            db.SaveChanges();
        }
    }
}
