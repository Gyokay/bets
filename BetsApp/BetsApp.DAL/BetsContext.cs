﻿using System.Data.Entity;

using BetsApp.DAL.Models;

namespace BetsApp.DAL
{
    public class BetsContext : DbContext 
    {
        public BetsContext() : base("BetsDB")
        {

        }

        public virtual DbSet<Sport> Sports { get; set; }

        public virtual DbSet<Event> Events { get; set; }

        public virtual DbSet<Match> Matches { get; set; }

        public virtual DbSet<Bet> Bets { get; set; }

        public virtual DbSet<Odd> Odds { get; set; }
    }
}
