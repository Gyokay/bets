﻿namespace BetsApp.DAL
{
    internal static class ModifiedElementsTracker
    {
        internal const int MaxElementCount = 1000;
        internal static int ModifiedElementsCount { get; set; }
    }
}
