﻿using System;
using BetsApp.DAL.Interfaces;
using System.Linq;
using BetsApp.DAL.Models;
using BetsApp.DTOs;
using System.Collections.Generic;

namespace BetsApp.DAL.Repositories
{
    public class OddRepository : IOddRepository
    {
        private BetsContext db;
        private DatabaseSaver dbSaver;

        public OddRepository(BetsContext db)
        {
            this.db = db;
            this.dbSaver = new DatabaseSaver(this.db);
        }

        public void Add(string id, string betId, string name, string value, string specialBetValue)
        {
            id = id.Trim();
            betId = betId.Trim();
            name = name.Trim();
            value = value.Trim();

            var odd = new Odd()
            {
                OddId = id,
                BetId = betId,
                Name = name,
                Value = value,
                SpecialBetValue = specialBetValue
            };

            db.Odds.Add(odd);

            dbSaver.Save();
        }

        private Odd GetSingleElementById(string id)
        {
            return db.Odds.Single(x => x.OddId == id);
        }

        public bool IsChanged(string id, string betId, string name, string value, string specialBetValue)
        {
            id = id.Trim();
            betId = betId.Trim();
            name = name.Trim();
            value = value.Trim();

            var odd = GetSingleElementById(id);

            if (odd == null)
                throw new ArgumentNullException("No such odd");

            if (odd.BetId != betId) return true;
            if (odd.Name != name) return true;
            if (odd.Value != value) return true;
            if (odd.SpecialBetValue != specialBetValue) return true;

            return false;
        }

        public bool ContainsById(string id)
        {
            id = id.Trim();

            return db.Odds.Any(x => x.OddId == id);
        }

        public void UpdateById(string id, string betId, string name, string value, string specialBetValue)
        {
            id = id.Trim();
            betId = betId.Trim();
            name = name.Trim();
            value = value.Trim();

            var odd = GetSingleElementById(id);

            if (odd == null)
                throw new ArgumentNullException("No such odd");

            db.Entry(odd).Property(o => o.BetId).CurrentValue = betId;
            db.Entry(odd).Property(o => o.Name).CurrentValue = name;
            db.Entry(odd).Property(o => o.Value).CurrentValue = value;
            db.Entry(odd).Property(o => o.SpecialBetValue).CurrentValue = specialBetValue;

            dbSaver.Save();
        }

        public IList<OddDTO> GetOddsStartingBefore(DateTime maxDateTime)
        {
            var odds = (from s in db.Sports
                        from e in s.Events
                        from m in e.Matches
                        where m.StartDate.CompareTo(maxDateTime) <= 0 &&
                        m.StartDate.CompareTo(DateTime.Now) >= 0
                        from b in m.Bets
                        from o in b.Odds
                        select new OddDTO()
                        {
                            OddId = o.OddId,
                            BetId = o.BetId,
                            Name = o.Name,
                            Value = o.Value,
                            SpecialBetValue = o.SpecialBetValue
                        })
                        .Distinct()
                        .ToList();

            return odds;
        }

        public IDictionary<string, OddDTO> GetAll()
        {
            var allOdds = (from o in db.Odds
                           select o).ToList();

            return allOdds.ToDictionary(o => o.OddId, o => new OddDTO
            {
                BetId = o.BetId,
                Name = o.Name,
                Value = o.Value,
                SpecialBetValue = o.SpecialBetValue
            });
        }

        public OddDTO GetById(string id)
        {
            var o = db.Odds.Single(x => x.OddId == id);

            return new OddDTO
            {
                OddId = o.OddId,
                BetId = o.BetId,
                Name = o.Name,
                Value = o.Value,
                SpecialBetValue = o.SpecialBetValue
            };
        }
    }
}
