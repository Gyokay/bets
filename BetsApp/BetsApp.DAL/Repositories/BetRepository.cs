﻿using System;
using BetsApp.DAL.Interfaces;
using System.Linq;
using BetsApp.DAL.Models;
using BetsApp.DTOs;
using System.Collections.Generic;

namespace BetsApp.DAL.Repositories
{
    public class BetRepository : IBetRepository
    {
        private BetsContext db;
        private DatabaseSaver dbSaver;

        public BetRepository(BetsContext db)
        {
            this.db = db;
            this.dbSaver = new DatabaseSaver(this.db);
        }

        public void Add(string id, string matchId, string name, bool isLive)
        {
            id = id.Trim();
            matchId = matchId.Trim();
            name = name.Trim();

            var bet = new Bet
            {
                BetId = id,
                MatchId = matchId,
                Name = name,
                IsLive = isLive
            };

            db.Bets.Add(bet);

            dbSaver.Save();
        }

        public bool ContainsById(string id)
        {
            id = id.Trim();

            return db.Bets.Any(x => x.BetId == id);
        }

        private Bet GetSingleItemById(string id)
        {
            return db.Bets.Single(x => x.BetId == id);
        }

        public bool IsChanged(string id, string matchId, string name, bool isLive)
        {
            id = id.Trim();
            matchId = matchId.Trim();
            name = name.Trim();

            var bet = GetSingleItemById(id);

            if (bet == null)
                throw new NullReferenceException("No such item in db");

            if (bet.MatchId != matchId) return true;
            if (bet.Name != name) return true;
            if (bet.IsLive != isLive) return true;

            return false;
        }

        public void UpdateById(string id, string matchId, string name, bool isLive)
        {
            id = id.Trim();
            matchId = matchId.Trim();
            name = name.Trim();

            var bet = GetSingleItemById(id);

            if (bet == null)
                throw new NullReferenceException("No such item in db");

            db.Entry(bet).Property(b => b.MatchId).CurrentValue = matchId;
            db.Entry(bet).Property(b => b.Name).CurrentValue = name;
            db.Entry(bet).Property(b => b.IsLive).CurrentValue = isLive;

            dbSaver.Save();
        }

        public IList<BetDTO> GetBetsStartingBefore(DateTime maxDateTime)
        {
            var bets = (from s in db.Sports
                        from e in s.Events
                        from m in e.Matches
                        where m.StartDate.CompareTo(maxDateTime) <= 0 &&
                        m.StartDate.CompareTo(DateTime.Now) >= 0
                        from b in m.Bets
                        select new BetDTO()
                        {
                            BetId = b.BetId,
                            MatchId = b.MatchId,
                            Name = b.Name,
                            IsLive = b.IsLive
                        })
                        .Distinct()
                        .ToList();

            return bets;
        }

        public IDictionary<string, BetDTO> GetAll()
        {
            var allBets = (from b in db.Bets
                           select b).ToList();

            return allBets.ToDictionary(b => b.BetId, b => new BetDTO
            {
                MatchId = b.MatchId,
                Name = b.Name,
                IsLive = b.IsLive
            });
        }

        public BetDTO GetById(string id)
        {
            var b = db.Bets.Single(x => x.BetId == id);

            return new BetDTO
            {
                BetId = b.BetId,
                MatchId = b.MatchId,
                Name = b.Name,
                IsLive = b.IsLive
            };
        }
    }
}
