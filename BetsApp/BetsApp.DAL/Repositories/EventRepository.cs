﻿using BetsApp.DAL.Interfaces;
using System.Linq;
using BetsApp.DAL.Models;
using System;
using BetsApp.DTOs;
using System.Collections.Generic;

namespace BetsApp.DAL.Repositories
{
    public class EventRepository : IEventRepository
    {
        private BetsContext db;
        private DatabaseSaver dbSaver;

        public EventRepository(BetsContext db)
        {
            this.db = db;
            this.dbSaver = new DatabaseSaver(this.db);
        }

        public void Add(string id, string sportId, string name, bool isLive, string categoryId)
        {
            id = id.Trim();
            sportId = sportId.Trim();
            name = name.Trim();
            categoryId = categoryId.Trim();

            var ev = new Event
            {
                EventId = id,
                SportId = sportId,
                Name = name,
                IsLive = isLive,
                CategoryId = categoryId
            };

            db.Events.Add(ev);

            dbSaver.Save();
        }

        public void UpdateById(string id, string sportId, string name, bool isLive, string categoryId)
        {
            id = id.Trim();
            sportId = sportId.Trim();
            name = name.Trim();
            categoryId = categoryId.Trim();

            var ev = db.Events.Single(x => x.EventId == id);

            if (ev == null)
                throw new NullReferenceException("No such item in db");

            db.Entry(ev).Property(e => e.SportId).CurrentValue = sportId;
            db.Entry(ev).Property(e => e.Name).CurrentValue = name;
            db.Entry(ev).Property(e => e.IsLive).CurrentValue = isLive;
            db.Entry(ev).Property(e => e.CategoryId).CurrentValue = categoryId;

            dbSaver.Save();
        }

        private Event GetSingleItemById(string id)
        {
            return db.Events.Single(x => x.EventId == id);
        }

        public bool IsChanged(string id, string sportId, string name, bool isLive, string categoryId)
        {
            id = id.Trim();
            sportId = sportId.Trim();
            name = name.Trim();
            categoryId = categoryId.Trim();

            var ev = GetSingleItemById(id);

            if (ev == null)
                throw new NullReferenceException("No such item in db");

            if (ev.SportId != sportId) return true;
            if (ev.Name != name) return true;
            if (ev.IsLive != isLive) return true;
            if (ev.CategoryId != categoryId) return true;

            return false;
        }

        public bool ContainsById(string id)
        {
            id = id.Trim();

            return db.Events.Any(x => x.EventId == id);
        }

        public IList<EventDTO> GetEventsStartingBefore(DateTime maxDateTime)
        {
            var events = (from s in db.Sports
                          from e in s.Events
                          from m in e.Matches
                          where m.StartDate.CompareTo(maxDateTime) <= 0 &&
                          m.StartDate.CompareTo(DateTime.Now) >= 0
                          where m.Bets.Count > 0
                          select new EventDTO
                          {
                              EventId = e.EventId,
                              SportId = e.SportId,
                              Name = e.Name
                          })
                               .Distinct()
                               .ToList();

            return events;
        }

        public EventDTO GetById(string id)
        {
            var e = db.Events.Single(x => x.EventId == id);

            return new EventDTO
            {
                EventId = e.EventId,
                SportId = e.SportId,
                Name = e.Name
            };
        }
    }
}
