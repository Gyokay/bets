﻿using BetsApp.DAL.Interfaces;
using BetsApp.DAL.Models;
using System.Linq;
using System;
using BetsApp.DTOs;
using System.Collections.Generic;

namespace BetsApp.DAL.Repositories
{
    public class SportRepository : ISportRepository
    {
        private BetsContext db;
        private DatabaseSaver dbSaver;

        public SportRepository(BetsContext db)
        {
            if (db == null)
                throw new NullReferenceException("DbContext");

            this.db = db;
            this.dbSaver = new DatabaseSaver(this.db);
        }

        public void UpdateById(string id, string name)
        {
            id = id.Trim();
            name = name.Trim();

            var sport = GetSingleItemById(id);

            if (sport == null)
                throw new NullReferenceException("No such sport in db");

            db.Entry(sport).Property(s => s.Name).CurrentValue = name;

            dbSaver.Save();
        }

        private Sport GetSingleItemById(string id)
        {
            return db.Sports.Single(x => x.SportId == id);
        }

        public bool IsChanged(string id, string name)
        {
            id = id.Trim();
            name = name.Trim();

            var sport = GetSingleItemById(id);

            if (sport == null)
                throw new NullReferenceException("No such sport in db");

            if (sport.Name != name) return true;

            return false;
        }

        public void Add(string id, string name)
        {
            id = id.Trim();
            name = name.Trim();

            var sport = new Sport
            {
                SportId = id,
                Name = name
            };

            db.Sports.Add(sport);

            dbSaver.Save();
        }

        public bool ContainsById(string id)
        {
            id = id.Trim();

            return db.Sports.Any(x => x.SportId == id);
        }

        public IList<SportDTO> GetSportsStartingBefore(DateTime maxDateTime)
        {
            var sports = (from s in db.Sports
                          from e in s.Events
                          from m in e.Matches
                          where m.StartDate.CompareTo(maxDateTime) <= 0 &&
                          m.StartDate.CompareTo(DateTime.Now) >= 0
                          where m.Bets.Count > 0
                          select new SportDTO()
                          {
                              Name = s.Name,
                              SportId = s.SportId
                          })
                          .Distinct()
                          .ToList();

            return sports;
        }

        public SportDTO GetById(string id)
        {
            var s = db.Sports.Single(x => x.SportId == id);

            return new SportDTO
            {
                SportId = s.SportId,
                Name = s.Name
            };
        } 
    }
}
