﻿using System;

using BetsApp.DAL.Interfaces;
using System.Linq;
using BetsApp.DAL.Models;
using System.Collections.Generic;
using BetsApp.DTOs;

namespace BetsApp.DAL.Repositories
{
    public class MatchRepository : IMatchRepository
    {
        private BetsContext db;
        private DatabaseSaver dbSaver;

        public MatchRepository(BetsContext db)
        {
            this.db = db;
            this.dbSaver = new DatabaseSaver(this.db);
        }

        public void Add(string id, string eventId, string name, DateTime startDate, string matchType)
        {
            id = id.Trim();
            eventId = eventId.Trim();
            name = name.Trim();
            matchType = matchType.Trim();

            var match = new Match
            {
                MatchId = id,
                EventId = eventId,
                Name = name,
                StartDate = startDate,
                MatchType = matchType
            };

            db.Matches.Add(match);

            dbSaver.Save();
        }

        public void UpdateById(string id, string eventId, string name, DateTime startDate, string matchType)
        {
            id = id.Trim();
            eventId = eventId.Trim();
            name = name.Trim();
            matchType = matchType.Trim();

            var match = GetSingleElementById(id);

            if (match == null)
                throw new NullReferenceException("No such item db");

            db.Entry(match).Property(m => m.EventId).CurrentValue = eventId;
            db.Entry(match).Property(m => m.Name).CurrentValue = name;
            db.Entry(match).Property(m => m.StartDate).CurrentValue = startDate;
            db.Entry(match).Property(m => m.MatchType).CurrentValue = matchType;

            dbSaver.Save();
        }

        private Match GetSingleElementById(string id)
        {
            return db.Matches.Single(x => x.MatchId == id);
        }

        public bool IsChanged(string id, string eventId, string name, DateTime startDate, string matchType)
        {
            id = id.Trim();
            eventId = eventId.Trim();
            name = name.Trim();
            matchType = matchType.Trim();

            var match = GetSingleElementById(id);

            if (match == null)
                throw new NullReferenceException("No such element id db");

            if (match.EventId != eventId) return true;
            if (match.Name != name) return true;
            if (match.StartDate != startDate) return true;
            if (match.MatchType != matchType) return true;

            return false;
        }

        public bool ContainsById(string id)
        {
            id = id.Trim();

            return db.Matches.Any(x => x.MatchId == id);
        }

        public IList<MatchDTO> GetMatchesStartingBefore(DateTime maxDateTime)
        {
            var matches = (from s in db.Sports
                           from e in s.Events
                           from m in e.Matches
                           where m.StartDate.CompareTo(maxDateTime) <= 0 &&
                           m.StartDate.CompareTo(DateTime.Now) >= 0
                           where m.Bets.Count > 0
                           select new MatchDTO
                           {
                               MatchId = m.MatchId,
                               EventId = m.EventId,
                               Name = m.Name,
                               StartDate = m.StartDate
                           })
                           .Distinct()
                           .ToList();

            return matches;
        }

        public MatchDTO GetById(string id)
        {
            var m = db.Matches.Single(x => x.MatchId == id);

            return new MatchDTO
            {
                MatchId = m.MatchId,
                EventId = m.EventId,
                Name = m.Name,
                StartDate = m.StartDate
            };
        }
    }
}
