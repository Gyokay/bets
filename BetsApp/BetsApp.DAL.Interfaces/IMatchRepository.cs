﻿using BetsApp.DTOs;
using System;
using System.Collections.Generic;

namespace BetsApp.DAL.Interfaces
{
    public interface IMatchRepository
    {
        void Add(string id, string eventId, string name, DateTime startDate, string matchType);

        void UpdateById(string id, string eventId, string name, DateTime startDate, string matchType);

        bool IsChanged(string id, string eventId, string name, DateTime startDate, string matchType);

        bool ContainsById(string id);

        IList<MatchDTO> GetMatchesStartingBefore(DateTime maxDateTime);

        MatchDTO GetById(string id);
    }
}
