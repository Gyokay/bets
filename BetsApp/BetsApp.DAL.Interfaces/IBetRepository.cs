﻿using BetsApp.DTOs;
using System;
using System.Collections.Generic;

namespace BetsApp.DAL.Interfaces
{
    public interface IBetRepository
    {
        void Add(string id, string matchId, string name, bool isLive);

        void UpdateById(string id, string matchId, string name, bool isLive);

        bool IsChanged(string id, string matchId, string name, bool isLive);

        bool ContainsById(string id);

        IList<BetDTO> GetBetsStartingBefore(DateTime maxTime);

        IDictionary<string, BetDTO> GetAll();

        BetDTO GetById(string id);
    }
}
