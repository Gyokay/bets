﻿using BetsApp.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetsApp.DAL.Interfaces
{
    public interface IEventRepository
    {
        void Add(string id, string sportId, string name, bool isLive, string categoryId);

        void UpdateById(string id, string sportId, string name, bool isLive, string categoryId);

        bool IsChanged(string id, string sportId, string name, bool isLive, string categoryId);

        bool ContainsById(string id);

        IList<EventDTO> GetEventsStartingBefore(DateTime maxTime);

        EventDTO GetById(string id);
    }
}
