﻿using BetsApp.DTOs;
using System;
using System.Collections.Generic;

namespace BetsApp.DAL.Interfaces
{
    public interface ISportRepository
    {
        void Add(string id, string name);

        void UpdateById(string id, string name);

        bool IsChanged(string id, string name);

        bool ContainsById(string id);

        IList<SportDTO> GetSportsStartingBefore(DateTime maxDateTime);

        SportDTO GetById(string id); 
    }
}
