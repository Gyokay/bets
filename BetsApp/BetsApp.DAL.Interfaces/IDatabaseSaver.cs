﻿namespace BetsApp.DAL.Interfaces
{
    public interface IDatabaseSaver
    {
        void SaveImmediately();
    }
}
