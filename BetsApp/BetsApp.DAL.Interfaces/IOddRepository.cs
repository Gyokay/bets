﻿using BetsApp.DTOs;
using System;
using System.Collections.Generic;

namespace BetsApp.DAL.Interfaces
{
    public interface IOddRepository
    {
        void Add(string id, string betId, string name, string value, string specialBetValue);

        void UpdateById(string id, string betId, string name, string value, string specialBetValue);

        bool IsChanged(string id, string betId, string name, string value, string specialBetValue);

        bool ContainsById(string id);

        IList<OddDTO> GetOddsStartingBefore(DateTime maxTime);

        IDictionary<string, OddDTO> GetAll();

        OddDTO GetById(string id);
    }
}
