﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BetsApp.DataParsers.XMLModels
{
    public class Odd : IEqualityComparer<Odd>
    {
        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public string Id { get; set; }

        [XmlAttribute("Value")]
        public string Value { get; set; }

        [XmlAttribute("SpecialBetValue")]
        public string SpecialBetValue;

        public bool Equals(Odd x, Odd y)
        {
            if (x.Id != y.Id) return false;
            if (x.Name != y.Name) return false;
            if (x.Value != y.Value) return false;
            if (x.SpecialBetValue != y.SpecialBetValue) return false;

            return true;
        }

        public int GetHashCode(Odd obj)
        {
            throw new NotImplementedException();
        }
    }
}