﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BetsApp.DataParsers.XMLModels
{
    public class Bet
    {
        [XmlElement("Odd")]
        public List<Odd> Odds { get; set; }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public string Id { get; set; }

        [XmlAttribute("IsLive")]
        public bool IsLive { get; set; }
    }
}
