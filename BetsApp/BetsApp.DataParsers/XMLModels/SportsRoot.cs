﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BetsApp.DataParsers.XMLModels
{
    [XmlRoot("XmlSports")]
    public class SportsRoot
    {
        [XmlElement("Sport")]
        public List<Sport> Sports { get; set; }
    }
}
