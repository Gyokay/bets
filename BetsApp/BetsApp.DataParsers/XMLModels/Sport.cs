﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace BetsApp.DataParsers.XMLModels
{
    public class Sport
    {
        [XmlElement("Event")]
        public List<Event> Events { get; set; }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public string Id { get; set; }
    }
}