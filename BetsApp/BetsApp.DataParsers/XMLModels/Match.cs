﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace BetsApp.DataParsers.XMLModels
{
    public class Match
    {
        [XmlElement("Bet")]
        public List<Bet> Bets { get; set; }

        [XmlAttribute("Name")]
        public string Name { get; set; }

        [XmlAttribute("ID")]
        public string Id { get; set; }

        [XmlAttribute("StartDate")]
        public DateTime StartDate { get; set; }

        [XmlAttribute("MatchType")]
        public string MatchType { get; set; }
    }
}