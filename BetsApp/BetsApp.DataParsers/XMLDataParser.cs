﻿using System.IO;
using System.Xml.Serialization;

using BetsApp.DataParsers.Interfaces;
using BetsApp.DataParsers.XMLModels;

namespace BetsApp.DataParsers
{
    public class XMLDataParser : IDataParser
    {
        public object ParseData(string xmlString)
        {
            using (var reader = new StringReader(xmlString))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(SportsRoot));
                SportsRoot root = (SportsRoot)serializer.Deserialize(reader);

                return root;
            }

        }
    }
}
