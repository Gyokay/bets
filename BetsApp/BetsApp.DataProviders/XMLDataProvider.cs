﻿using System;
using BetsApp.DataProviders.Interfaces;
using System.Xml;

namespace BetsApp.DataProviders
{
    public class XMLDataProvider : IDataProvider
    {
        public string GetData(string uri)
        {
            XmlDocument xmlDoc = new XmlDocument();
            xmlDoc.Load(uri);

            return xmlDoc.OuterXml;
        }
    }
}
