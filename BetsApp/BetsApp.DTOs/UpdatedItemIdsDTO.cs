﻿using System.Collections.Generic;

namespace BetsApp.DTOs
{
    public class UpdatedItemIdsDTO
    {
        public IList<string> SportIds { get; set; }

        public IList<string> EventIds { get; set; }

        public IList<string> MatchIds { get; set; }

        public IList<string> BetIds { get; set; }

        public IList<string> OddIds { get; set; }
    }
}
