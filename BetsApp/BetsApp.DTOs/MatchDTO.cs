﻿using System;
using System.Collections.Generic;

namespace BetsApp.DTOs
{
    public class MatchDTO
    {
        public string MatchId { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public string EventId { get; set; }
    }
}
