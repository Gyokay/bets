﻿using System.Collections.Generic;

namespace BetsApp.DTOs
{
    public class BetDTO
    {
        public string BetId { get; set; }

        public string Name { get; set; }

        public bool IsLive { get; set; }

        public string MatchId { get; set; }
    }
}
