﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetsApp.DTOs
{
    public class SportsRootDTO
    {
        public IList<SportDTO> Sports { get; set; }

        public IList<EventDTO> Events { get; set; }

        public IList<MatchDTO> Matches { get; set; }

        public IList<BetDTO> Bets { get; set; }

        public IList<OddDTO> Odds { get; set; }
    }
}
