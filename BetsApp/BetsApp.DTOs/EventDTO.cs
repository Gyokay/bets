﻿using System.Collections.Generic;

namespace BetsApp.DTOs
{
    public class EventDTO
    {
        public string EventId { get; set; }

        public string Name { get; set; }

        public string SportId { get; set; }
    }
}
