﻿namespace BetsApp.DTOs
{
    public class OddDTO
    {
        public string OddId { get; set; }

        public string Name { get; set; }

        public string Value { get; set; }

        public string SpecialBetValue { get; set; }

        public string BetId { get; set; }
    }
}