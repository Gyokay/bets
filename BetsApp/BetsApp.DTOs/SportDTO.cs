﻿using System.Collections.Generic;

namespace BetsApp.DTOs
{
    public class SportDTO
    {
        public string SportId { get; set; }

        public string Name { get; set; }
    }
}
