﻿namespace BetsApp.DataProviders.Interfaces
{
    public interface IDataProvider
    {
        string GetData(string uri);
    }
}
