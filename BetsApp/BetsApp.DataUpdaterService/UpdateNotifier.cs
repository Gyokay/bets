﻿using BetsApp.DTOs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace BetsApp.DataUpdaterService
{
    public class UpdateNotifier
    {
        private readonly string userName;
        private readonly string password;
        private readonly string apiBaseUri;
        private readonly string apiNotificationPath;
        private string apiToken;

        public UpdateNotifier(
            string userName,
            string password,
            string apiBaseUri,
            string apiNotificationPath
            )
        {
            this.userName = userName;
            this.password = password;
            this.apiBaseUri = apiBaseUri;
            this.apiNotificationPath = apiNotificationPath;
        }

        public void Notifie(UpdatedItemIdsDTO ids)
        {
            if (apiToken == null)
            {
                this.apiToken = GetAPIToken(userName, password, apiBaseUri).Result;
            }

            string response = PostRequest(this.apiToken, apiBaseUri, apiNotificationPath, ids).Result;
        }

        private async Task<string> PostRequest(string token, string apiBaseUri, string requestPath, UpdatedItemIdsDTO data)
        {
            using (var client = new HttpClient())
            {
                //setup client
                client.BaseAddress = new Uri(apiBaseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Add("Authorization", "Bearer " + token);

                StringContent content = new StringContent(JsonConvert.SerializeObject(data), Encoding.UTF8, "application/json");
                
                // HTTP POST
                HttpResponseMessage response = await client.PostAsync(requestPath, content);
                
                var responseString = await response.Content.ReadAsStringAsync();
                return responseString;
            }
        }

        private async Task<string> GetAPIToken(string userName, string password, string apiBaseUri)
        {
            using (var client = new HttpClient())
            {
                //setup client
                client.BaseAddress = new Uri(apiBaseUri);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //setup login data
                var formContent = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("username", userName),
                    new KeyValuePair<string, string>("password", password),
                });

                //send request
                HttpResponseMessage responseMessage = await client.PostAsync("/Token", formContent);

                //get access token from response body
                var responseJson = await responseMessage.Content.ReadAsStringAsync();
                var jObject = JObject.Parse(responseJson);
                return jObject.GetValue("access_token").ToString();
            }
        }
    }
}
