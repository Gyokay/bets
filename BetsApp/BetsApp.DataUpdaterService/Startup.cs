﻿using BetsApp.BLL;
using BetsApp.DAL;
using BetsApp.DAL.Repositories;
using BetsApp.DataParsers;
using BetsApp.DataProviders;

namespace BetsApp.DataUpdaterService
{
    public class Startup
    {
        static void Main(string[] args)
        {
            var dbContext = new BetsContext();

            dbContext.Configuration.AutoDetectChangesEnabled = false;
            dbContext.Configuration.ValidateOnSaveEnabled = false;

            var sportRepo = new SportRepository(dbContext);
            var eventRepo = new EventRepository(dbContext);
            var matchRepo = new MatchRepository(dbContext);
            var betRepo = new BetRepository(dbContext);
            var oddRepo = new OddRepository(dbContext);

            var idCollectior = new UpdatedItemIdsCollector();

            var dataUpdaterService = new BLL.DataUpdaterService(
                sportRepo,
                eventRepo,
                matchRepo,
                betRepo,
                oddRepo,
                idCollectior
                );

            var dataProvider = new XMLDataProvider();
            var dataParser = new XMLDataParser();

            var dbSaver = new DatabaseSaver(dbContext);


            const string userName = "admin@mail.com";
            const string password = "Admin12*";
            const string apiBaseUri = "http://localhost:2503";
            const string apiNotificationPath = "/api/Update";

            var updateNotifier = new UpdateNotifier(
                userName,
                password,
                apiBaseUri,
                apiNotificationPath
                );

            var service = new DataUpdaterServiceManager(
                dataProvider,
                dataParser,
                dataUpdaterService,
                dbSaver,
                updateNotifier
                    );

            const string url = "http://vitalbet.net/sportxml";

            service.StartService(url);
        }
    }
}
