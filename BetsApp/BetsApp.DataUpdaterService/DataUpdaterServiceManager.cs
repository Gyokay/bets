﻿using BetsApp.BLL.Interfaces;
using BetsApp.DAL.Interfaces;
using BetsApp.DataParsers.Interfaces;
using BetsApp.DataParsers.XMLModels;
using BetsApp.DataProviders.Interfaces;
using System;
using System.IO;
using System.Threading;

namespace BetsApp.DataUpdaterService
{
    public class DataUpdaterServiceManager : IDataUpdaterServiceManager
    {
        private readonly IDataProvider xmlDataProvider;
        private readonly IDataParser xmlDataParser;
        private readonly IDataUpdaterService dataUpdaterService;
        private readonly IDatabaseSaver dbSaver;
        private readonly UpdateNotifier updateNotifier;
        private bool isRunning;
        private bool isInitialRun;

        public DataUpdaterServiceManager(
            IDataProvider xmlDataProvider,
            IDataParser xmlDataParser,
            IDataUpdaterService dataUpdaterService,
            IDatabaseSaver dbSaver,
            UpdateNotifier updateNotifier
            )
        {
            if (xmlDataProvider == null)
                throw new ArgumentNullException("xmlDataProvider");

            if (xmlDataParser == null)
                throw new ArgumentNullException("xmlDataParser");

            if (dataUpdaterService == null)
                throw new ArgumentNullException("dataUpdateService");

            if (dbSaver == null)
                throw new ArgumentNullException("DatabaseSaver");

            if (updateNotifier == null)
                throw new ArgumentNullException("UpdateNotifier");

            this.xmlDataProvider = xmlDataProvider;
            this.xmlDataParser = xmlDataParser;
            this.dataUpdaterService = dataUpdaterService;
            this.dbSaver = dbSaver;
            this.updateNotifier = updateNotifier;
            isInitialRun = true;
        }

        public void StartService(string uri)
        {
            this.isRunning = true;

            while (isRunning)
            {
                string xmlString;

                try
                {
                    xmlString = xmlDataProvider.GetData(uri);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    continue;
                }

                using (var reader = new StringReader(xmlString))
                {
                    var root = (SportsRoot)xmlDataParser.ParseData(xmlString);

                    dataUpdaterService.StartUpdateChain(root);
                }

                dbSaver.SaveImmediately();

                var changedItems = dataUpdaterService.GetChangedItems();

                if (!isInitialRun)
                {
                    // send items to app api endpoint
                    updateNotifier.Notifie(changedItems);
                }

                dataUpdaterService.ResetIdCollection();

                isInitialRun = false;

                Console.WriteLine("*");

                Thread.Sleep(60000);

            }
        }

        public void StopService()
        {
            this.isRunning = false;
        }
    }
}
