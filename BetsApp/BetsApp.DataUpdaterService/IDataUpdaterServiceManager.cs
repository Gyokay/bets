﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BetsApp.DataUpdaterService
{
    public interface IDataUpdaterServiceManager
    {
        void StartService(string uri);
    }
}
