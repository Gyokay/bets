﻿using BetsApp.BLL.Interfaces;
using BetsApp.DAL.Interfaces;
using BetsApp.DataParsers.XMLModels;
using BetsApp.DTOs;
using System;
using System.Collections.Generic;

namespace BetsApp.BLL
{
    public class DataUpdaterService : IDataUpdaterService
    {
        private readonly ISportRepository sportRepository;
        private readonly IEventRepository eventRepository;
        private readonly IMatchRepository matchRepository;
        private readonly IBetRepository betRepository;
        private readonly IOddRepository oddRepository;

        private IDictionary<string, MatchDTO> matchCache;
        private IDictionary<string, BetDTO> betCache;
        private IDictionary<string, OddDTO> oddCache;

        private readonly IUpdatedItemIdsCollector idCollector;

        public DataUpdaterService(
            ISportRepository sportRepository,
            IEventRepository eventRepository,
            IMatchRepository matchRepository,
            IBetRepository betRepository,
            IOddRepository oddRepository,
            IUpdatedItemIdsCollector idCollector
            )
        {
            if (sportRepository == null)
                throw new ArgumentNullException("SportsRepository");

            if (eventRepository == null)
                throw new ArgumentNullException("EventRepository");

            if (matchRepository == null)
                throw new ArgumentNullException("MatchRepository");

            if (betRepository == null)
                throw new ArgumentNullException("BetRepository");

            if (oddRepository == null)
                throw new ArgumentNullException("OddRopesitory");

            if (idCollector == null)
                throw new ArgumentNullException("IdCollectior");

            this.sportRepository = sportRepository;
            this.eventRepository = eventRepository;
            this.matchRepository = matchRepository;
            this.betRepository = betRepository;
            this.oddRepository = oddRepository;

            this.idCollector = idCollector;

            this.matchCache = new Dictionary<string, MatchDTO>();
            this.betCache = new Dictionary<string, BetDTO>();
            this.oddCache = new Dictionary<string, OddDTO>();
        }

        public void StartUpdateChain(object rootData)
        {
            var root = (SportsRoot)rootData;

            BuildCache();

            UpdateSports(root.Sports, this.sportRepository);
        }

        private void UpdateSports(IList<Sport> sportList, ISportRepository sportRepository)
        {
            foreach (Sport sport in sportList)
            {
                if (!(sportRepository.ContainsById(sport.Id)))
                {
                    sportRepository.Add(sport.Id, sport.Name);

                    idCollector.AddSportId(sport.Id);
                }
                else
                {
                    if (sportRepository.IsChanged(sport.Id, sport.Name))
                    {
                        sportRepository.UpdateById(sport.Id, sport.Name);

                        idCollector.AddSportId(sport.Id);
                    }
                }

                UpdateEvens(sport.Id, sport.Events, this.eventRepository);
            }
        }

        private void UpdateEvens(string sportId, IList<Event> eventList, IEventRepository eventRepository)
        {
            foreach (Event ev in eventList)
            {
                if (!(eventRepository.ContainsById(ev.Id)))
                {
                    eventRepository.Add(ev.Id, sportId, ev.Name, ev.IsLive, ev.CategoryId);

                    idCollector.AddEventId(ev.Id);
                }
                else
                {
                    if (eventRepository.IsChanged(ev.Id, sportId, ev.Name, ev.IsLive, ev.CategoryId))
                    {
                        eventRepository.UpdateById(ev.Id, sportId, ev.Name, ev.IsLive, ev.CategoryId);

                        idCollector.AddEventId(ev.Id);
                    }
                }

                UpdateMatches(ev.Id, ev.Matches, this.matchRepository);
            }
        }

        private void UpdateMatches(string eventId, IList<Match> matchList, IMatchRepository matchRepository)
        {
            foreach (Match match in matchList)
            {
                if (!(matchRepository.ContainsById(match.Id)))
                {
                    matchRepository.Add(match.Id, eventId, match.Name, match.StartDate, match.MatchType);

                    idCollector.AddMatchId(match.Id);
                }
                else
                {
                    if (matchRepository.IsChanged(match.Id, eventId, match.Name, match.StartDate, match.MatchType))
                    {
                        matchRepository.UpdateById(match.Id, eventId, match.Name, match.StartDate, match.MatchType);

                        idCollector.AddMatchId(match.Id);
                    }
                }

                UpdateBets(match.Id, match.Bets, this.betRepository);
            }
        }

        private void UpdateBets(string matchId, IList<Bet> betList, IBetRepository betRepository)
        {
            foreach (Bet bet in betList)
            {
                var currentBetObj = new BetDTO
                {
                    MatchId = matchId.Trim(),
                    Name = bet.Name.Trim(),
                    IsLive = bet.IsLive
                };

                if (!this.betCache.ContainsKey(bet.Id))
                {
                    betRepository.Add(bet.Id, matchId, bet.Name, bet.IsLive);

                    this.betCache.Add(bet.Id, currentBetObj);

                    idCollector.AddBetId(bet.Id);
                }
                else
                {
                    var cachedBetObj = this.betCache[bet.Id];

                    var isEqual = true;

                    if (cachedBetObj.MatchId != currentBetObj.MatchId) isEqual = false;
                    if (cachedBetObj.Name != currentBetObj.Name) isEqual = false;
                    if (cachedBetObj.IsLive != currentBetObj.IsLive) isEqual = false;

                    if (!isEqual)
                    {
                        betRepository.UpdateById(bet.Id, matchId, bet.Name, bet.IsLive);

                        this.betCache[bet.Id] = currentBetObj;

                        idCollector.AddBetId(bet.Id);
                    }
                }

                UpdateOdds(bet.Id, bet.Odds, this.oddRepository);
            }
        }

        private void UpdateOdds(string betId, IList<Odd> oddList, IOddRepository oddRepository)
        {
            foreach (Odd odd in oddList)
            {
                var currentOddObj = new OddDTO
                {
                    BetId = betId.Trim(),
                    Name = odd.Name.Trim(),
                    Value = odd.Value.Trim(),
                    SpecialBetValue = odd.SpecialBetValue,
                };

                if (!this.oddCache.ContainsKey(odd.Id))
                {
                    oddRepository.Add(odd.Id, betId, odd.Name, odd.Value, odd.SpecialBetValue);

                    this.oddCache.Add(odd.Id, currentOddObj);

                    idCollector.AddOddId(odd.Id);
                }
                else
                {
                    var cachedOddObj = this.oddCache[odd.Id];

                    var isEqual = true;

                    if (cachedOddObj.BetId != currentOddObj.BetId) isEqual = false;
                    if (cachedOddObj.Name != currentOddObj.Name) isEqual = false;
                    if (cachedOddObj.Value != currentOddObj.Value) isEqual = false;
                    if (cachedOddObj.SpecialBetValue != currentOddObj.SpecialBetValue) isEqual = false;

                    if (!isEqual)
                    {
                        oddRepository.UpdateById(odd.Id, betId, odd.Name, odd.Value, odd.SpecialBetValue);

                        this.oddCache[odd.Id] = currentOddObj;

                        idCollector.AddOddId(odd.Id);
                    }
                }
            }
        }

        public UpdatedItemIdsDTO GetChangedItems()
        {
            return this.idCollector.GetCollectedItems();
        }

        public void ResetIdCollection()
        {
            this.idCollector.Reset();
        }

        private void BuildCache()
        {
            if (this.matchCache.Count > 0) return;
            if (this.oddCache.Count > 0) return;

            this.betCache = betRepository.GetAll();

            this.oddCache = oddRepository.GetAll();
        }
    }
}
