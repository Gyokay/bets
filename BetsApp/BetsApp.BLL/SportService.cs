﻿using BetsApp.BLL.Interfaces;
using BetsApp.DAL.Interfaces;
using BetsApp.DTOs;
using System;

namespace BetsApp.BLL
{
    public class SportService : ISportService
    {
        private readonly ISportRepository sportRepo;
        private readonly IEventRepository eventRepo;
        private readonly IMatchRepository matchRepo;
        private readonly IBetRepository betRepo;
        private readonly IOddRepository oddRepo;

        public SportService(
            ISportRepository sportRepo,
            IEventRepository eventRepo,
            IMatchRepository matchRepo,
            IBetRepository betRepo,
            IOddRepository oddRepo
            )
        {
            this.sportRepo = sportRepo;
            this.eventRepo = eventRepo;
            this.matchRepo = matchRepo;
            this.betRepo = betRepo;
            this.oddRepo = oddRepo;
        }

        public SportsRootDTO GetSports()
        {
            const int addItionalHours = 24;

            var maxDateTime = DateTime.Now.AddHours(addItionalHours);

            var sports = sportRepo.GetSportsStartingBefore(maxDateTime);
            var events = eventRepo.GetEventsStartingBefore(maxDateTime);
            var matches = matchRepo.GetMatchesStartingBefore(maxDateTime);
            var bets = betRepo.GetBetsStartingBefore(maxDateTime);
            var odds = oddRepo.GetOddsStartingBefore(maxDateTime);

            var sportsRootDTO = new SportsRootDTO();
            sportsRootDTO.Sports = sports;
            sportsRootDTO.Events = events;
            sportsRootDTO.Matches = matches;
            sportsRootDTO.Bets = bets;
            sportsRootDTO.Odds = odds;

            return sportsRootDTO;
        }

        public SportDTO GetSportById(string id)
        {
            return sportRepo.GetById(id);
        }

        public EventDTO GetEventById(string id)
        {
            return eventRepo.GetById(id);
        }

        public MatchDTO GetMatchById(string id)
        {
            return matchRepo.GetById(id);
        }

        public BetDTO GetBetById(string id)
        {
            return betRepo.GetById(id);
        }

        public OddDTO GetOddById(string id)
        {
            return oddRepo.GetById(id);
        }
    }
}
