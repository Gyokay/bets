﻿using BetsApp.BLL.Interfaces;
using BetsApp.DTOs;
using System.Collections.Generic;

namespace BetsApp.BLL
{
    public class UpdatedItemIdsCollector : IUpdatedItemIdsCollector
    {
        private UpdatedItemIdsDTO idCollection;

        public UpdatedItemIdsCollector()
        {
            this.Reset();
        }

        public void AddBetId(string id)
        {
            this.idCollection.BetIds.Add(id);
        }

        public void AddEventId(string id)
        {
            this.idCollection.EventIds.Add(id);
        }

        public void AddMatchId(string id)
        {
            this.idCollection.MatchIds.Add(id);
        }

        public void AddOddId(string id)
        {
            this.idCollection.OddIds.Add(id);
        }

        public void AddSportId(string id)
        {
            this.idCollection.SportIds.Add(id);
        }

        public UpdatedItemIdsDTO GetCollectedItems()
        {
            return this.idCollection;
        }

        public void Reset()
        {
            this.idCollection = new UpdatedItemIdsDTO();
            this.idCollection.SportIds = new List<string>();
            this.idCollection.EventIds = new List<string>();
            this.idCollection.MatchIds = new List<string>();
            this.idCollection.BetIds = new List<string>();
            this.idCollection.OddIds = new List<string>();
        }
    }
}
