﻿using BetsApp.DTOs;

namespace BetsApp.BLL.Interfaces
{
    public interface IDataUpdaterService
    {
        void StartUpdateChain(object rootData);

        UpdatedItemIdsDTO GetChangedItems();

        void ResetIdCollection();
    }
}
