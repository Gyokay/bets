﻿using BetsApp.DTOs;

namespace BetsApp.BLL.Interfaces
{
    public interface IUpdatedItemIdsCollector
    {
        void AddSportId(string id);

        void AddEventId(string id);

        void AddMatchId(string id);

        void AddBetId(string id);

        void AddOddId(string id);

        UpdatedItemIdsDTO GetCollectedItems();

        void Reset();
    }
}
