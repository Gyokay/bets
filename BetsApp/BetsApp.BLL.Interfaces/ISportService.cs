﻿using BetsApp.DTOs;
using System.Collections.Generic;

namespace BetsApp.BLL.Interfaces
{
    public interface ISportService
    {
        SportsRootDTO GetSports();

        SportDTO GetSportById(string id);

        EventDTO GetEventById(string id);

        MatchDTO GetMatchById(string id);

        BetDTO GetBetById(string id);

        OddDTO GetOddById(string id);
    }
}
